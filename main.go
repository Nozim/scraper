package main

import (
	"fmt"
	"regexp"

	"github.com/nozim/scraper/indexer"
	"github.com/nozim/scraper/scraper"
	"github.com/prometheus/common/log"
)

func propgururules() []scraper.ScrapingRule {
	rules := make([]scraper.ScrapingRule, 8)
	rules[0] = scraper.ScrapingRule{"address", "div[@class='listing_info']/div[@class='info1']/p[2]", scraper.DefaultExtractor}
	rules[1] = scraper.ScrapingRule{"agent_name", "div[@class='listing_info']/div[@class='info1']/p[3]", scraper.AgentNameExtractor}
	rules[2] = scraper.ScrapingRule{"agent_phone", "div[@class='listing_info']/div[@class='info1']/p[3]", scraper.AgentPhoneExtractor}
	rules[3] = scraper.ScrapingRule{"property_type", "div[@class='listing_info']/div[@class='info1']/p[1]", scraper.DefaultExtractor}
	rules[4] = scraper.ScrapingRule{"created_at", "div[@class='listing_info']/div[@class='info1']/p[4]", scraper.DateExtractor}
	rules[5] = scraper.ScrapingRule{"origin", "div[@class='listing_info']/a/@href", scraper.DefaultExtractor}
	rules[6] = scraper.ScrapingRule{"price", "div[@class='morelinks']//div[@class='floatL details']/p[2]", scraper.PriceExtractor}
	rules[7] = scraper.ScrapingRule{"area", "div[@class='listing_info']/div[@class='info2']/p[3]", scraper.DefaultExtractor}
	//itemsSelector := "//ul/li[@class='listing_item']"
	return rules
}

func ipropertyrules() []scraper.ScrapingRule {
	rules := make([]scraper.ScrapingRule, 8)
	rules[0] = scraper.ScrapingRule{"address", "div[@class='main-container']/div/div[@class='article-left']/p[1]", scraper.DefaultExtractor}
	rules[1] = scraper.ScrapingRule{"agent_name", "div[@class='main-container']//div[@class='article-right']/p[1]", scraper.DefaultExtractor}
	rules[2] = scraper.ScrapingRule{"agent_phone", "div[@class='main-container']//div[@class='article-right']/p[3]/a/@onclick", func(s string) string {
		re := regexp.MustCompile(`.*'([\d\+]*)'.*`)
		return re.FindStringSubmatch(s)[1]
	}}
	rules[3] = scraper.ScrapingRule{"property_type", "div[@class='main-container']/div/div[@class='article-left']/p[2]", scraper.DefaultExtractor}
	rules[4] = scraper.ScrapingRule{"created_at", "div[@class='main-container']/div/div[@class='article-left']/p[5]", scraper.DefaultExtractor}
	rules[5] = scraper.ScrapingRule{"origin", "div[@class='main-container']/div/div[@class='article-left']/h2/a/@href", func(s string) string {
		return "http://iproperty.com.sg" + s
	}}
	rules[6] = scraper.ScrapingRule{"price", "div[@class='main-container']//div[@class='article-right']/h2", scraper.DefaultExtractor}
	rules[7] = scraper.ScrapingRule{"area", "div[@class='main-container']/div/div[@class='article-left']/p[3]", scraper.DefaultExtractor}

	return rules
}

// fetch
// scrape
// geocode
// index

func processIproperty(url string) int {
	esclient := indexer.GetESClient()
	fetcher := scraper.PageFetcher{"iproperty", url}
	page := fetcher.Fetch()
	itemsSelector := "//ul[@class='search-results']/li[@class='search-listing']"
	rules := ipropertyrules()
	scr := scraper.Scraper{itemsSelector, rules}
	rawads := scr.Scrape(page)
	ads := scraper.BuildPosts(rawads)
	fmt.Println(ads)
	rec, err := esclient.Index(ads)
	if err != nil {
		log.Error(err)
		return 0
	}
	return rec
}

func main() {
	template := "http://www.iproperty.com.sg/rent/property/?pg=%d"
	//fetcher.Start(processIproperty)
	for i := 1; i < 2; i++ {
		url := fmt.Sprintf(template, i)
		fmt.Println(fmt.Sprintf("Processing %s", url))
		processIproperty(url)
		//	time.Sleep(3 * time.Second)
	}
}
