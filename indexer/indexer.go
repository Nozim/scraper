package indexer

import (
	"crypto/md5"
	"encoding/json"
	"flag"
	"fmt"
	"log"

	elastigo "github.com/mattbaird/elastigo/lib"
	"github.com/nozim/scraper/scraper"
)

var (
	eshost *string = flag.String("host", "dockerhost", "Elasticsearch Server Host Address")
)

type ElasticClient struct {
	Connection *elastigo.Conn
}

func (c ElasticClient) Index(ads []scraper.Post) (int, error) {
	//	c := elastigo.NewConn()
	//	c.Domain = *eshost
	writes := 0
	for _, ad := range ads {
		id := generateId(ad.Origin)
		_, err := c.Connection.Index("ads", "ad", id, nil, ad)
		if err != nil {
			log.Fatal(err)
			return writes, err
		} else {
			writes++
		}
	}
	c.Connection.Flush()
	return writes, nil
}

func generateId(s string) string {
	return fmt.Sprintf("%x", md5.Sum([]byte(s)))
}

func (c ElasticClient) SearchPosts(s string) []scraper.Post {
	out, err := c.Connection.Search("ads", "ad", nil, s)
	if err != nil {
		log.Fatal(err)
	}

	ads := make([]scraper.Post, len(out.Hits.Hits))
	for index, hit := range out.Hits.Hits {
		var ad scraper.Post
		err = json.Unmarshal(*hit.Source, &ad)
		ads[index] = ad
	}
	return ads
}

func GetESClient() ElasticClient {
	c := elastigo.NewConn()
	c.Domain = *eshost
	return ElasticClient{c}
}

//func Query(location scraper.Location) []scraper.Post {
//	client := GetESClient()
//	searchJson := fmt.Sprintf(`{"query": {
//		"match_all": {}
//	}
//	, "filter": {
//		"geo_distance": {
//			"distance": "1km",
//
//			"location": {
//				"lat": %f,
//				"lon": %f
//			}
//		}
//	}}`, location.Lat, location.Lng)
//	return client.SearchPosts(searchJson)
//}
