package scraper

import (
	"fmt"
	"io/ioutil"
	"testing"
)

//func PropGuruScraper() Scraper {
//	itemsSelector := "//ul/li[@class='listing_item']"
//	rules := make([]ScrapingRule, 8)
//	rules[0] = ScrapingRule{"address", "div[@class='listing_info']/div[@class='info1']/p[2]", func(s string) string {
//		return DefaultExtractor(s) + ", Singapore"
//	}}
//	rules[1] = ScrapingRule{"agent_name", "div[@class='listing_info']/div[@class='info1']/p[3]", AgentNameExtractor}
//	rules[2] = ScrapingRule{"agent_phone", "div[@class='listing_info']/div[@class='info1']/p[3]", AgentPhoneExtractor}
//	rules[3] = ScrapingRule{"property_type", "div[@class='listing_info']/div[@class='info1']/p[1]", DefaultExtractor}
//	rules[4] = ScrapingRule{"created_at", "div[@class='listing_info']/div[@class='info1']/p[4]", DateExtractor}
//	rules[5] = ScrapingRule{"origin", "div[@class='listing_info']/a/@href", DefaultExtractor}
//	rules[6] = ScrapingRule{"price", "div[@class='morelinks']//div[@class='floatL details']/p[2]", PriceExtractor}
//	rules[7] = ScrapingRule{"area", "div[@class='listing_info']/div[@class='info2']/p[3]", DefaultExtractor}
//	//	rules[8] = ScrapingRule{"agency", "div[@class='listing_info']/div[@class='info2']/p[3]", DefaultExtractor}
//	//	rules[9] = ScrapingRule{"agent_reg_num", "div[@class='listing_info']/div[@class='info2']/p[3]", DefaultExtractor}
//	//	rules[10] = ScrapingRule{"agent_reg_num", "div[@class='listing_info']/div[@class='info2']/p[3]", DefaultExtractor}
//	return Scraper{itemsSelector, rules}
//}
//
//func TestScraper(t *testing.T) {
//	str := `<ul>
//	<li><p>foo</p></li>
//	<li><p>test2</p></li>
//	<ul>`
//	rules := make([]ScrapingRule, 1)
//	rules[0] = ScrapingRule{"bar", "p/text()", DefaultExtractor}
//	scraper := Scraper{"//ul/li", rules}
//	s := scraper.Scrape(str)
//	fmt.Println(s)
//	assert.Equal(t, "foo", s[0]["bar"])
//	assert.Equal(t, "test2", s[1]["bar"])
//
//}

//func TestScrapePage(t *testing.T) {
//	str, err := ioutil.ReadFile("/home/nozim/propguru/foo.html")
//	if err != nil {
//		fmt.Println(err)
//	}
//	s := PropGuruScraper()
//	res := s.Scrape(string(str))
//	fmt.Println(res)
//	assert.Equal(t, 49, len(res))
//	assert.Equal(t, res[0]["address"], "6 Jalan Tua Kong, Singapore")
//	assert.Equal(t, res[0]["agent_name"], "chandra Das s/o p ramadas (chan)")
//	assert.Equal(t, res[0]["agent_phone"], "+6593866197")
//	assert.Equal(t, res[0]["property_type"], "Condominium")
//	assert.Equal(t, res[0]["created_at"], "Mar 16, 2015")
//	assert.Equal(t, res[0]["origin"], "http://www.propertyguru.com.sg/listing/18887966/for-rent-crescendo-park")
//	assert.Equal(t, res[0]["price"], "2,400 monthly")
//	assert.Equal(t, res[0]["area"], "850 sqft / 78.97 sqm (built-up)")
//
//	assert.Equal(t, res[1]["address"], "52 Bayshore Park, Singapore")
//	assert.Equal(t, res[1]["agent_name"], "Celine Cheng")
//	assert.Equal(t, res[1]["agent_phone"], "+6596428280")
//	assert.Equal(t, res[1]["property_type"], "Condominium")
//	assert.Equal(t, res[1]["created_at"], "Mar 16, 2015")
//	assert.Equal(t, res[1]["origin"], "http://www.propertyguru.com.sg/listing/18834711/for-rent-bayshore-park")
//	assert.Equal(t, res[1]["price"], "3,300 monthly")
//	assert.Equal(t, res[1]["area"], "936 sqft / 86.96 sqm (built-up)")
//
//}

//func TestAgentExtractor(t *testing.T) {
//	in := "\n                                                                                                                                                                                                                                                                                                                                                  Marketed by chandra Das s/o p ramadas (chan) - Call +6593866197                            "
//	assert.Equal(t, agentNameExtractor(in), "chandra Das s/o p ramadas (chan)")
//}
//

func TestLargeHtmlPage(t *testing.T) {
	dat, _ := ioutil.ReadFile("./propertyguru/sample.html")
	_ = Post{"6 Jalan Tua Kong, Singapore",
		"chandra Das s/o p ramadas (chan)",
		"+6593866197",
		"Condominium",
		"Rent",
		"Mar 16, 2015",
		"http://www.propertyguru.com.sg/listing/18887966/for-rent-crescendo-park",
		"2400 / month",
		"",
		Location{},
	}
	content := string(dat)
	itemsSelector := "ul.listing_list li"
	rules := make([]ScrapingRule, 8)
	rules[0] = ScrapingRule{"address", "div[@class='listing_info']/div[@class='info1']/p[2]", DefaultExtractor}
	rules[1] = ScrapingRule{"agent_name", "div[@class='listing_info']/div[@class='info1']/p[3]", AgentNameExtractor}
	rules[2] = ScrapingRule{"agent_phone", "div[@class='listing_info']/div[@class='info1']/p[3]", AgentPhoneExtractor}
	rules[3] = ScrapingRule{"property_type", "div[@class='listing_info']/div[@class='info1']/p[1]", DefaultExtractor}
	rules[4] = ScrapingRule{"created_at", "div[@class='listing_info']/div[@class='info1']/p[4]", DateExtractor}
	rules[5] = ScrapingRule{"origin", "div[@class='listing_info']/a/@href", DefaultExtractor}
	rules[6] = ScrapingRule{"price", "div[@class='morelinks']//div[@class='floatL details']/p[2]", PriceExtractor}
	rules[7] = ScrapingRule{"area", "div[@class='listing_info']/div[@class='info2']/p[3]", DefaultExtractor}

	propguruscraper := Scraper{itemsSelector, rules}
	res := propguruscraper.Scrape(content)
	//	assert.Equal(t, len(res), 193)
	fmt.Println(res)
	//assert.Equal(t, res[0].AgentName, correct.AgentName)
}

func TestScrapingUrls(t *testing.T) {
	dat, _ := ioutil.ReadFile("./propertyguru/sample.html")

	linkHarvester := LinkReaper{}

}
