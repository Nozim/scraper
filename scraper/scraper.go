package scraper

import (
	"fmt"
	"strings"

	"gopkg.in/xmlpath.v2"
)

type Location struct {
	Lat float64 `json:"lat"`
	Lng float64 `json:"lon"`
}

type Scrapable interface {
	Scrape(string) []Post
	BuildPosts(postsmap []map[string]string) []Post
}

type ScrapingRule struct {
	Name      string
	Selector  string
	Extractor func(string) string
}

type Scraper struct {
	ItemsSelector string
	Rules         []ScrapingRule
}

type Post struct {
	Address      string   `json:"address"`
	AgentName    string   `json:"agent_name"`
	AgentPhone   string   `json:"agent_phone"`
	PropertyType string   `json:"property_type"`
	AdType       string   `json:"ad_type"`
	CreatedAt    string   `json:"created_at"`
	Origin       string   `json:"origin"`
	Price        string   `json:"price"`
	Area         string   `json:"area"`
	Coordinates  Location `json:"location"`
}

func (scraper Scraper) Scrape(content string) []map[string]string {
	itemspath, err := xmlpath.Compile(scraper.ItemsSelector)
	if err != nil {
		fmt.Println(err)
		return nil
	}

	root, err := xmlpath.ParseHTML(strings.NewReader(content))
	if err != nil {
		fmt.Println(err)
		return nil
	}

	rootIter := itemspath.Iter(root)

	res := []map[string]string{}
	for rootIter.Next() {
		node := rootIter.Node()
		itemdetails := make(map[string]string)

		for _, rule := range scraper.Rules {
			itempath := xmlpath.MustCompile(rule.Selector)
			val, _ := itempath.String(node)
			itemdetails[rule.Name] = rule.Extractor(val)
		}

		res = append(res, itemdetails)
	}
	return res
}

func BuildPosts(postsmap []map[string]string) []Post {
	posts := make([]Post, len(postsmap))
	for index, item := range postsmap {
		posts[index] = buildPost(item)
	}
	return posts
}

func buildPost(item map[string]string) Post {
	return Post{
		item["address"] + ", Singapore",
		item["agent_name"],
		item["agent_phone"],
		item["property_type"],
		item["ad_type"],
		item["created_at"],
		item["origin"],
		item["price"],
		item["area"], Location{0, 0}}
}
