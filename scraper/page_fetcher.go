package scraper

import (
	"io/ioutil"
	"net/http"
)

type PageFetcher struct {
	PageType string
	Url      string
}

func (f PageFetcher) Fetch() string {
	return GetPage(f.Url)
}

func GetPage(url string) string {
	client := &http.Client{}
	req, _ := http.NewRequest("GET", url, nil)
	req.Header.Set("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10; rv:33.0) Gecko/20100101 Firefox/33.0")
	resp, _ := client.Do(req)
	defer resp.Body.Close()
	body, _ := ioutil.ReadAll(resp.Body)
	return string(body)
}
