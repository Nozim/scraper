package scraper

import (
	"fmt"
	"regexp"
	"strings"
)

func RegexpExtractor(pattern string, s string) string {
	re := regexp.MustCompile(pattern)
	matches := re.FindStringSubmatch(s)
	if len(matches) < 2 {
		return ""
	}
	return matches[1]
}

func AgentNameExtractor(s string) string {
	return RegexpExtractor(`.*Marketed by (.+) - Call.* $`, s)
}

func AgentPhoneExtractor(s string) string {
	return RegexpExtractor(`.*Call ([\d\+]*)`, s)
}

func DefaultExtractor(s string) string {
	return strings.TrimSpace(s)
}

func DateExtractor(s string) string {
	fmt.Println(s)
	return RegexpExtractor(`.*isted on (.*)`, s)
}

func PriceExtractor(s string) string {
	return RegexpExtractor(".*S. (.* monthly).*", s)
}
